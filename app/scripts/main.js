console.log('\'Allo \'Allo!');

// Uncomment to enable Bootstrap tooltips
// https://getbootstrap.com/docs/4.0/components/tooltips/#example-enable-tooltips-everywhere
// $(function () { $('[data-toggle="tooltip"]').tooltip(); });

// Uncomment to enable Bootstrap popovers
// https://getbootstrap.com/docs/4.0/components/popovers/#example-enable-popovers-everywhere
// $(function () { $('[data-toggle="popover"]').popover(); });

//swiper
var swiper = new Swiper('.swiper-container', {
  slidesPerView: 3,
  spaceBetween: 30,
  slidesPerGroup: 3,
  loop: true,
  loopFillGroupWithBlank: true,

  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
  breakpoints: {
    750: {
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
    }
  }
});

/* SCRIPT JQUERY PARA TROCAR DE COR NAVBAR */
$(window).scroll(function () {
  if ($(this).scrollTop() > 300) {
    $('.menu').css('background-color', '#528fdb')
  } else {
    $('.menu').css('background-color', 'transparent')
  }
});
